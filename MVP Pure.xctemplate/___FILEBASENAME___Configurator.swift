//___FILEHEADER___

//MARK: - Configrator -
import Foundation

protocol ___VARIABLE_productName___Configurator {
    func configure(vc:___VARIABLE_productName___VC)
}

class ___VARIABLE_productName___ConfiguratorImplementation: ___VARIABLE_productName___Configurator {
    
    func configure(vc:___VARIABLE_productName___VC) {
        let view = vc
        let router = ___VARIABLE_productName___RouterImplementation(vc: view)
        let interactor = ___VARIABLE_productName___InteractorImplementation()
        let presenter = ___VARIABLE_productName___PresenterImplementation(view: view, router: router,interactor:interactor)
        vc.presenter = presenter
    }
    
}
