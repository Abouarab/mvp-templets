//___FILEHEADER___

//MARK: - VC
import UIKit

protocol ___VARIABLE_productName___View: BaseView {

}

final class ___VARIABLE_productName___VC: BaseVC {
    
    //MARK: - IBOutlets -
    
    //MARK: - Properities -
    var configurator: ___VARIABLE_productName___Configurator?
    var presenter: ___VARIABLE_productName___Presenter?
    
    // MARK: - UIViewController Events
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.viewDidLoad()
    }
    
    //MARK: - Design Methods -
    
    
    
    //MARK: - IBActions -
    
}
extension ___VARIABLE_productName___VC: ___VARIABLE_productName___View {}
