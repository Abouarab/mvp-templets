//___FILEHEADER___

//MARK:- VC
import UIKit

protocol ___VARIABLE_productName___View: BaseView {
    func reloadList()
}

final class ___VARIABLE_productName___VC: BaseVC {
    
    //MARK:- IBOutlets -
    @IBOutlet weak private var collectionView: UICollectionView!
    
    //MARK:- Properities -
    var configurator: ___VARIABLE_productName___Configurator?
    var presenter: ___VARIABLE_productName___Presenter?
    private let cellIdentifier: String = <#Cell Name#>
    
    // MARK: - UIViewController Events
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.viewDidLoad()
        self.setupCollectionView()
    }
    
    //MARK:- Design Methods -
    private func setupCollectionView() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(UINib(nibName: self.cellIdentifier, bundle: nil), forCellWithReuseIdentifier: self.cellIdentifier)
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    //MARK:- IBActions -
    
    
}
extension ___VARIABLE_productName___VC: ___VARIABLE_productName___View {
    func reloadList() {
        self.collectionView.reloadData()
    }
}

//MARK:- CollectionView -
extension ___VARIABLE_productName___VC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.presenter?.getCount ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath)
        return cell
    }
}
extension ___VARIABLE_productName___VC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath)
    }
}
extension ___VARIABLE_productName___VC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        <#code#>
    }
}
