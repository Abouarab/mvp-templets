//___FILEHEADER___

//MARK:- Presenter -
import Foundation

protocol ___VARIABLE_productName___Presenter {
    //MARK:- Properities -
    var getCount: Int {get}
    
    //MARK:- Methods -
    func viewDidLoad()
    
}

class ___VARIABLE_productName___PresenterImplementation {
    
    //MARK:- Properities -
    fileprivate weak var view: ___VARIABLE_productName___View?
    internal let router: ___VARIABLE_productName___Router
    internal let interactor : ___VARIABLE_productName___Interactor
    private var items: [String] = []

    //MARK:- Initializer -
    init(view: ___VARIABLE_productName___View,router: ___VARIABLE_productName___Router,interactor:___VARIABLE_productName___Interactor) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
    
    //MARK:- Logic Methods -
    
    
    //MARK:- Network Methods -

}
extension ___VARIABLE_productName___PresenterImplementation:___VARIABLE_productName___Presenter {
    
    //MARK:- Properities -
    var getCount: Int {
        return self.items.count
    }
    
    //MARK:- Methods -
    func viewDidLoad() {
        
    }
}
