//___FILEHEADER___

//MARK:- Router -
import UIKit

protocol ___VARIABLE_productName___Router {
    
}

class ___VARIABLE_productName___RouterImplementation {
    
    //MARK:- Properities -
    fileprivate weak var vc: ___VARIABLE_productName___VC?
    
    //MARK:- Initializer -
    init(vc: ___VARIABLE_productName___VC) {
        self.vc = vc
    }
    
}
extension ___VARIABLE_productName___RouterImplementation: ___VARIABLE_productName___Router {
    
}
