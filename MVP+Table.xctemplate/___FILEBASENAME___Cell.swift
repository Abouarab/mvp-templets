//___FILEHEADER___


//MARK:- Cell
import UIKit

protocol ___VARIABLE_productName___CellView {
    
}

class ___VARIABLE_productName___Cell: UITableViewCell {
    
    //MARK:- IBOutlets -
    
    
    //MARK:- properities -
    static let identifier: String = "___VARIABLE_productName___Cell"
    
    //MARK:- Overriden Methods-
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupDesign()
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.resetCellData()
    }
    
    
    //MARK:- Design Methods -
    private func setupDesign() {
        self.selectionStyle = .none
    }
    private func resetCellData() {
        
    }
    
}
extension ___VARIABLE_productName___Cell: ___VARIABLE_productName___CellView {
    
}
