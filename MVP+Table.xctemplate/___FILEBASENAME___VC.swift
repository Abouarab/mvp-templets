//___FILEHEADER___

//MARK:- VC
import UIKit

protocol ___VARIABLE_productName___View: BaseView {
    func reloadList()
    func reloadListWithAnimation()
}

final class ___VARIABLE_productName___VC: BaseVC {
    
    //MARK:- IBOutlets -
    @IBOutlet weak private var tableView: UITableView!
    
    //MARK:- Properities -
    var configurator: ___VARIABLE_productName___Configurator?
    var presenter: ___VARIABLE_productName___Presenter?
    private let cellIdentifier: String = ___VARIABLE_productName___Cell.identifier
    
    // MARK: - UIViewController Events
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.viewDidLoad()
        self.setupTableView()
    }
    
    //MARK:- Design Methods -
    private func setupTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: self.cellIdentifier, bundle: nil), forCellReuseIdentifier: self.cellIdentifier)
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.tableFooterView = UIView(frame: .zero)
    }
    
    //MARK:- IBActions -
    
    
}
extension ___VARIABLE_productName___VC: ___VARIABLE_productName___View {
    func reloadList() {
        self.tableView.reloadData()
    }
    func reloadListWithAnimation() {
        self.tableView.reloadData()
    }
}
//MARK:- TableView -
extension ___VARIABLE_productName___VC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.presenter?.getCount ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as! ___VARIABLE_productName___Cell
        self.presenter?.configure(cell: cell, at: indexPath.row)
        return cell
    }
}
extension ___VARIABLE_productName___VC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        <#code#>
    }
}
